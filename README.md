# Candidate-186063

## Technologies Used

* Cypress
* cypress-cucumber-preprocessor
   

## To Run

* Run `npm run test`

## Evidence

![alt text](Passing-Tests.png "Passing Tests")

