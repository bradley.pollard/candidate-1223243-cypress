Feature: Confirm whether a visa is required to visit the UK

  Scenario: Japan - Study - 7 month stay
    Given I search for gov uk visa checking service
    When I provide a nationality of "Japan"
    And I select the reason "Study"
    And I state I am intending to stay for 7 months
    Then I will be informed "You’ll need a visa to study in the UK"

  Scenario: Japan - Tourism
    Given I search for gov uk visa checking service
    When I provide a nationality of "Japan"
    And I select the reason "Tourism"
    Then I will be informed "You won’t need a visa to come to the UK"

  Scenario: Russia - Tourism - Not travelling with partner or family
    Given I search for gov uk visa checking service
    When I provide a nationality of "Russia"
    And I select the reason "Tourism"
    And I state "No" to travelling with a partner or visiting a partner or family
    Then I will be informed "You’ll need a visa to come to the UK"

  Scenario Outline: Individual tests combined
    Given I search for gov uk visa checking service
    When I provide a nationality of "<country>"
    And I select the reason "<reason>"
    And I state I am intending to stay for <length_of_stay> months
    And I state "<travelling_with>" to travelling with a partner or visiting a partner or family
    Then I will be informed "<message>"

    Examples:

      | country | reason  | length_of_stay | travelling_with | message                                 |
      | Japan   | Study   | 7              | NA              | You’ll need a visa to study in the UK   |
      | Japan   | Tourism | -1             | NA              | You won’t need a visa to come to the UK |
      | Russia  | Tourism | -1             | no              | You’ll need a visa to come to the UK    |