import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps";

const url = {
  getGovURL: (arg) => {
    return arg
  }
}



 
Given('I search for gov uk visa checking service', () => {
    cy.stub(url, 'getGovURL').returns('https://www.gov.uk/')
    cy.visit(url.getGovURL())
    cy.get('input[name="q"]').type('check uk visa{enter}')
    cy.get('#js-results > div > ol > li:nth-child(1) > a').click()
    cy.get('#get-started > a').click()
})

When('I provide a nationality of {string}', (nationality) => {
    cy.get('#response').select(nationality.toLowerCase())
    cy.get('#current-question > button').click()
})

And('I select the reason {string}', (reason) => {
    if (reason.toLowerCase() == "tourism") {
        cy.get('#response-0').click()
    } else {
        cy.get('#response-2').click()
    }
    cy.get('#current-question > button').click()
})

And('I state I am intending to stay for {int} months', (number) => {
    if (number == -1) {
    }else{
        if (number <= 6) {
            cy.get('#response-0').click()
        }else{
            cy.get('#response-1').click()

        }
        cy.get('#current-question > button').click()
    }

})

And('I state {string} to travelling with a partner or visiting a partner or family', (statement) => {
    if (statement == "NA") {
    }else{
        cy.get('#response-1').click()
        cy.get('#current-question > button').click()
    }

})

Then('I will be informed {string}', (message) => {
    cy.get('.gem-c-heading').should('contain', message)
})